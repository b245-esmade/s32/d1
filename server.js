
const http = require('http');

// Mock Database
let directory = [
	{
	'name': "Brandon",
	'email': "brandon@email.com"},
	{
	'name': "Jobert",
	'email': "jobertn@email.com"
	}
]


let port = 3000;

http.createServer( function(request,response){
	// Add route here for GET method that will retrieve the content of our DB

	if (request.url === '/users' && request.method ==='GET'){

		// set the status 200
		// application/json sets the response to JSON data type
		response.writeHead(200, {'Content-Type': 'application/json'})

		// input has to bve data type STRING hence the JSON.stringify(method)
		response.write(JSON.stringify(directory))
		// 
		response.end()

	}else if (request.url === '/users' && request.method === 'POST'){
		let requestBody = '';
		request.on('data', function(data){
			requestBody+=data;
		})

		request.on('end', function(){
			console.log(typeof requestBody);
			console.log(requestBody);

			requestBody =JSON.parse(requestBody);
			console.log(typeof requestBody);


			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email};
				
			console.log(newUser);

			directory.push(newUser);
			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end()
		})

	}


}).listen(port)

console.log(`Server is running at port ${port}!`)