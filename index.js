

//Node.js Routing w/ HTTP methods
// Introduction to Postman


const http = require('http');

const port = 4000;

http.createServer(function(request, response){
	if (request.url == '/items' && request.method =='GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Data retrieved from/items.')
	}

	if(request.url === '/items' && request.method =='POST'){
		response.writeHead(200, {'Content-Type':'text/plain'})
		response.end('Data to be sent to the database')
	}

}).listen(port);

console.log(`Server is running at ${port}!`)

